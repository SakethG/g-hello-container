FROM openjdk:17-jdk-slim

WORKDIR /

COPY . /

RUN ./gradlew build

ENV PORT 8080
EXPOSE $PORT

CMD java -jar build/libs/g-hello-0.0.1-SNAPSHOT.jar
